# encoding: utf-8
module ApplicationHelper
  def admin_nav
    navs = [
        ['宝贝管理', items_path],
        ['幻灯片', slides_path],
        ['其它设置', settings_path],
        ['复制代码', codes_path],
        ['预览', root_path]
    ]

    html = ''
    navs.each do |nav|
      html << "<li"
      html << ' class="active"' if nav.second.include?(controller.controller_name)
      html << "><a href='#{nav.second}'>#{nav.first}</a></li>"
    end
    html.html_safe
  end

  def admin_left_menu
    html = ''
    if @admin_left_menus
      @admin_left_menus.each do |menu|
        if menu.second.nil?
          html << "<li class='nav-header'>#{menu.first}</li>"
        else
          active_class = ''
          if menu[2]
            controller_name, action_name = menu[2].split('#')
            if controller_name == controller.controller_name and action_name == controller.action_name
              active_class = ' class="active"'
            end
          end
          html << "<li#{active_class}><a href='#{menu.second}'>#{menu.first}</a></li>"
        end
      end
    end
    html.html_safe
  end
end
