class HomeController < ApplicationController

  def index
    @items = Item.order('cell ASC, sort ASC')
  end
end