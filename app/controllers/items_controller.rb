# encoding: utf-8
class ItemsController < ApplicationController

  before_filter :left_menus
  before_filter :load_item, only: [:edit, :update, :destroy]

  def index
    @items = Item.order('sort ASC, cell ASC')
  end

  def new
    @item = Item.new
  end

  def update
    if @item.update_attributes(params[:item])
      flash[:success] = '修改成功'
      redirect_to action: :index
    else
      flash[:error] = '修改失败'
      render action: :edit
    end
  end

  def create
    @item = Item.new(params[:item])
    if @item.save
      flash[:success] = '添加成功'
      redirect_to action: :new
    else
      flash[:error] = '添加失败'
      render action: :new
    end
  end

  def destroy
    @item.destroy ? flash[:success] = '删除成功' : flash[:error] = '删除失败'
    redirect_to action: :index
  end

  def rank
    Item.new_rank(params)
    render :nothing => true
  end

  def left_menus
      @admin_left_menus = [
          ['宝贝管理', nil],
          ['宝贝列表', items_path, 'items#index'],
          ['添加宝贝', new_item_path, 'items#new']
      ]
  end

  def load_item
    @item = Item.find_by_id(params[:id])
    redirect_to action: :index if @item.nil?
  end
end
