class ApplicationController < ActionController::Base
  protect_from_forgery

  def left_menus
    @admin_left_menus = nil
  end

end
