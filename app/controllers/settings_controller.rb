# encoding: utf-8
class SettingsController < ApplicationController
  before_filter :load_setting, only: [:edit, :update, :destroy]
  before_filter :left_menus

  def index
    @settings = Setting.all
  end

  def new
    @setting = Setting.new
  end

  def update
    if @setting.update_attributes(params[:setting])
      flash[:success] = '修改成功'
      redirect_to action: :index
    else
      flash[:error] = '修改失败'
      render action: :edit
    end
  end

  def create
    @setting = Setting.new(params[:setting])
    if @setting.save
      flash[:success] = '添加成功'
      redirect_to action: :new
    else
      flash[:error] = '添加失败'
      render action: :new
    end
  end

  def destroy
    @setting.destroy ? flash[:success] = '删除成功' : flash[:error] = '删除失败'
    redirect_to action: :index
  end

  def load_setting
    @setting = Setting.find_by_id(params[:id])
    redirect_to action: :index if @setting.nil?
  end

  def left_menus
      @admin_left_menus = [
          ['其它设置', nil],
          ['配置列表', settings_path, 'settings#index'],
          ['新增配置', new_setting_path, 'settings#new']
      ]
  end
end