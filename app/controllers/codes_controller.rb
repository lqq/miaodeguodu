class CodesController < ApplicationController

  def index

  end

  def copy_left_tpl
    @items = Item.order('cell ASC, sort ASC')
    render layout: false
  end

  def copy_right_tpl
    @items = Item.order('cell ASC, sort ASC')
    render layout: false
  end

end