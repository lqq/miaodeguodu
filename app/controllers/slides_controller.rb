# encoding: utf-8
class SlidesController < ApplicationController
  before_filter :load_slide, only: [:edit, :update, :destroy]

  before_filter :left_menus

  def index
    @slides = Slide.order('rank ASC')
  end

  def new
    @slide = Slide.new
  end

  def update
    if @slide.update_attributes(params[:slide])
      flash[:success] = '修改成功'
      redirect_to action: :index
    else
      flash[:error] = '修改失败'
      render action: :edit
    end
  end

  def create
    @slide = Slide.new(params[:slide])
    if @slide.save
      flash[:success] = '添加成功'
      redirect_to action: :new
    else
      flash[:error] = '添加失败'
      render action: :new
    end
  end

  def destroy
    @slide.destroy ? flash[:success] = '删除成功' : flash[:error] = '删除失败'
    redirect_to action: :index
  end

  def load_slide
    @slide = Slide.find_by_id(params[:id])
    redirect_to action: :index if @slide.nil?
  end

  def left_menus
      @admin_left_menus = [
          ['幻灯片管理', nil],
          ['图片列表', slides_path, 'slides#index'],
          ['添加图片', new_slide_path, 'slides#new']
      ]
  end
end