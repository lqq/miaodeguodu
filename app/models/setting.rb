# encoding: utf-8
class Setting < ActiveRecord::Base
  attr_accessible :key, :value
  validates_presence_of :key, :value
  validates_uniqueness_of :key

  class << self
    def types
      [
          %w(公告图片 1),
          %w(收藏链接图片 2),
          %w(幻灯片下方图片 3),
      ]
    end

    def notice
      (setting = Setting.find_by_key(1)) ? setting.value : ''
    end

    def collect
      (setting = Setting.find_by_key(2)) ? setting.value : ''
    end

    def intro
      (setting = Setting.find_by_key(3)) ? setting.value : ''
    end
  end

  def name
    name =''
    Setting.types.each do |type|
      name = type.first if type.second.to_s == self.key.to_s
    end
    name
  end
end
