class Slide < ActiveRecord::Base
  attr_accessible :click_url, :image_url, :rank
  validates_presence_of :image_url, :rank

  def self.data_spm_anchor_id
    '11040qTt.1-fxdeC.3-6MqBQb'
  end

end
