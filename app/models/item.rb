class Item < ActiveRecord::Base
  attr_accessible :cell, :click_url, :image_url, :price, :sort, :short_title
  validates_presence_of :cell, :click_url, :image_url, :sort

  def left_background
    'http://img01.taobaocdn.com/imgextra/i1/112115781/T2J6BjXohNXXXXXXXX_!!112115781.gif'
  end

  def right_background
    'http://img01.taobaocdn.com/imgextra/i1/112115781/T2J6BjXohNXXXXXXXX_!!112115781.gif'
  end

  class << self
    def new_rank(params)
      update_rank(params['image1_urls'], 1)
      update_rank(params['image2_urls'], 2)
      update_rank(params['image3_urls'], 3)
      update_rank(params['image4_urls'], 4)
    end

    private
    def update_rank(image_urls, cell)
      return nil if image_urls.blank?
      sort = 1
      image_urls.split(',').each do |image_url|
        if (item = Item.find_by_image_url(image_url))
          item.update_attributes(cell: cell, sort: sort)
        end
        sort += 1
      end
    end
  end
end
