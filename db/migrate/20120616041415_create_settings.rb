class CreateSettings < ActiveRecord::Migration
  def change
    create_table :settings do |t|
      t.string :value
      t.integer :key

      t.timestamps
    end
  end
end
