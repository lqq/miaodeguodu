class CreateItems < ActiveRecord::Migration
  def change
    create_table :items do |t|
      t.string :click_url
      t.string :image_url
      t.decimal :price, :precision => 7, :scale => 2
      t.integer :sort
      t.integer :cell

      t.timestamps
    end
  end
end
