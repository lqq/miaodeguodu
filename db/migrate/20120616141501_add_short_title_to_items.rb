class AddShortTitleToItems < ActiveRecord::Migration
  def change
    add_column :items, :short_title, :string
  end
end
