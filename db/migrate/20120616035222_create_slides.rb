class CreateSlides < ActiveRecord::Migration
  def change
    create_table :slides do |t|
      t.string :image_url
      t.string :click_url
      t.integer :rank, :default => 1

      t.timestamps
    end
  end
end
